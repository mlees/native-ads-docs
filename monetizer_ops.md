# Monetizer Ops

All Native Ads ops should be broadcast with a `json` value of an `array` containing three elements:

- acc type (string)
- op name (string)
- op data (object)

`["acc_type", "op_name", {op_data}]`

## account_init

```
{
    "id": "native-ads",
    "json": [
        "monetizer",
        "account_init",
        {
            "enabled":true,
            "token": "@@000000021"
        }
    ]
}
```

### Properties

Initial properties can be set by passing them as key-value pairs in `op_data`. Pass an empty object `{}` to have the account set up with defaults. Refer to the [Monetizer Accounts doc](monetizer_accs.md) for a complete list of supported properties and their default values.


---

## set_props

```
{
    "id": "native-ads",
    "json": [
        "monetizer",
        "set_props",
        {
            "enabled": true,
            "min_bid": 10.000,
            "min_time_bid": 60
        }
    ]
}
```

## create_space

**Required fields:**
`space_name`: `string (255)` permlink, only letter, numbers and dashes
`ad_type`: `string` type of ad refer to [ad specs](ad.md) for supported ad types
`title`: `string (255)` title of space
`description`: `string (1024)` short description of ad space
`guidelines`: `string`

```
{
    "id": "native-ads",
    "json": [
        "monetizer",
        "create_space",
        {
            "space_name": "featured-post",
            "ad_type": "post_global",
            "title": "Site-Wide Featured Post",
            "description": "Get featured on the homepage and 'featured post' spot on every page of our site.",
            "guidelines": "Content must be relevant and well-written."
        }
    ]
}
```

## ad_approve

```
{
    "id": "native-ads",
    "json": [
        "monetizer",
        "ad_approve",
        {
            "start_time": "2020-09-26T22:50:00",
            "monetizer": "hive-133333",
            "advertizer": "imwatsi.test",
            "ad_space": "featured-post-of-the-day",
            "permlink": "cool-post-i-wanna-promote-5",
            "mod_notes": "testing"
        }
    ]
}
```

## ad_reject

```
{
    "id": "native-ads",
    "json": [
        "monetizer",
        "ad_reject",
        {
            "monetizer": "hive-133333",
            "advertizer": "imwatsi.test",
            "ad_space": "featured-post-of-the-day",
            "permlink": "cool-post-i-wanna-promote-3",
            "mod_notes": "testing"
        }
    ]
}
```

## set_space_props

```
{
    "id": "native-ads",
    "json": [
        "monetizer",
        "set_space_props",
        {
            "space_name": "featured-post-of-the-day",
            "ad_type": "post_global",
            "title": "Featured Post of the Day",
            "description": "Get featured on the homepage and 'featured post' spot on every page of our site.",
            "guidelines": "Content must be relevant and well-written."
        }
    ]
}
```

