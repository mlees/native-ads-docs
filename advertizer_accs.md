## Supported Properties for Monetizer Accounts

*name: type (default)*

- `profile`: `json object` ({})

The profile can contain keys such as `display_name`, `profile_picture`, `website` or `location`.