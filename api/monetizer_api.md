# Monetizer API

### get_account

#### Params

`name`: `string` name of Hive account

#### Example Payload
```
{
    "jsonrpc": "2.0",
    "method": "monetizer.get_account",
    "params": {"name": "hive-133333"},
    "id": 1
}
```

#### Expected Response

```
{
    "jsonrpc": "2.0",
    "result": {
        "created": "2020-09-22T08:38:33", "profile": "{}",
        "enabled": True,
        "token": "@@000000021",
        "ad_types": ["post_global"],
        "burn": False,
        "min_bid": null,
        "min_time_bid": null,
        "max_time_bid": null, "max_time_active": null, "scheduled_delay": 1440, "scheduled_timeout": null
    },
    "id": 1
}
```

---

### get_moderation_list

#### Params

`name`: `string` name of Hive account

#### Example Payload
```
{
    "jsonrpc": "2.0",
    "method": "monetizer.get_moderation_list",
    "params": {"name": "hive-133333"},
    "id": 1
}
```

#### Expected Response

```
{
    "jsonrpc": "2.0",
    "result": [
        {
            "created": "2020-09-24T14:50:15", "ad_type": "post_global", "content": 'A "test"... please ignore... [Interesting promo content here].',
            "properties": {},
            "is_deleted": False,
            "cache_timestamp": "2020-09-29T07:59:49",
            "cache_timestamp_accessed": "2020-09-29T07:59:49",
            "account": "imwatsi.test",
            "space_name": "featured-post-of-the-day",
            "permlink": "cool-post-i-wanna-promote-5",
            "time_units": 60,
            "bid_amount": 1.0,
            "start_time": null,
            "mod_notes": "",
            "pptu": 0.016666666666666666
        },
        {
            ...
        }
    ]
}
```

---

### get_spaces

#### Params

`name`: `string` name of Hive account

#### Example Payload
```
{
    "jsonrpc": "2.0",
    "method": "monetizer.get_spaces",
    "params": {"name": "hive-133333"},
    "id": 1
}
```

#### Expected Response

```
{
    "jsonrpc": "2.0",
    "result": [
        {
            "space_name": "featured-post-of-the-day",
            "ad_type": "post_global",
            "title": "Site-Wide Featured Post",
            "description": "Get featured on the homepage and 'featured post' spot on every page of our site.",
            "guidelines": "Content must be relevant and well-written."
        },
        {
            ...
        }
    ],
    "id": 1
}
```

---

### get_space_market

#### Params

`name`: `string` name of Hive account
`space_name`: `string` name of the ad space

#### Example Payload
```
{
    "jsonrpc": "2.0",
    "method": "monetizer.get_space_market",
    "params": {
        "name": "hive-133333",
        "space_name": "featured-post-of-the-day"
    },
    "id": 1
}
```

#### Expected Response

```
{
    "jsonrpc": "2.0",
    "result": [
        {
            "created": "2020-09-24T14:50:15",
            "ad_type": "post_global",
            "content": 'A "test"... please ignore... [Interesting promo content here].',
            "properties": "{}",
            "account": "imwatsi.test",
            "permlink": "cool-post-i-wanna-promote-5",
            "time_units": 60,
            "bid_amount": 1.0,
            "start_time": null,
            "mod_notes": "",
            "pptu": 0.016666666666666666
        },
        {
            "created": "2020-09-20T13:39:33",
            "ad_type": "post_global",
            "content": "This is a test... please ignore... Interesting promo content here. [UPDATED] again, and once more",
            "properties": "{}",
            "account": "imwatsi.test",
            "permlink": "cool-post-i-wanna-promote-3",
            "time_units": 120,
            "bid_amount": 0.1,
            "start_time": null,
            "mod_notes": "testing",
            "pptu": 0.0008333333333333333
        }
    ],
    "id": 1
}
```

### get_account_notifications

#### Params

`name`: `string` name of Hive account
`category`: `string` (optional) types of notifications ["error"]

#### Example Payload
```
{
    "jsonrpc": "2.0",
    "method": "monetizer.get_account_notifications",
    "params": {
        "name": "hive-133333",
        "category": "error"
    },
    "id": 1
}
```

#### Expected Response

```
{
    "jsonrpc": "2.0",
    "result": {
        "notifications": [
            {
                "created": "2020-09-26T22:26:27",
                "category": 0,
                "remark": "ad_approve: start_time can't be in the past"
            },
            {
                "created": "2020-09-26T21:51:00",
                "category": 0,
                "remark": "'scheduled_ads_delay' value must be at least 1440 minutes"
            },
            {
                "created": "2020-09-26T21:44:09",
                "category": 0,
                "remark": "ad_approve: start_time can't be in the past"
            },
            {
                ...
            }
        ],
        "last_read": None
    },
    'id': 1
}
```

### get_live_ads

#### Params

`name`: `string` name of Hive account
`space_name`: `string` name of the ad space

#### Example Payload
```
{
    "jsonrpc": "2.0",
    "method": "monetizer.get_live_ads",
    "params": {
        "name": "hive-133333",
        "space_name": "featured-post-of-the-day"
    },
    "id": 1
}
```

#### Expected Response

```
{
    "jsonrpc": "2.0",
    "result": [
        {
            "created": "2020-09-24T14:50:15",
            "ad_type": "post_global",
            "content": 'A "test"... please ignore... [Interesting promo content here].',
            "properties": "{}",
            "account": "imwatsi.test",
            "permlink": "cool-post-i-wanna-promote-5", "time_units": 53,
            "bid_amount": 1.0,
            "start_time": "2020-09-29T20:02:00",
            "mod_notes": "testing",
            "pptu": 0.018867924528301886
        }
    ],
    "id": 1
}
```