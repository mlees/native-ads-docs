# Advertizer API

### get_account

#### Params

`name`: `string` name of Hive account

#### Example Payload
```
{
    "jsonrpc": "2.0",
    "method": "advertizer.get_account",
    "params": {"name": "imwatsi.test"},
    "id": 1
}
```

#### Expected Response
```
{
    "jsonrpc": "2.0",
    "result": {
        "created": "2020-09-19T18:10:48",
        "profile": '{"location": "Earth"}'
    },
    "id" : 1
}
```

### get_ad_posts

#### Params

`name`: `string` name of Hive account
`status`: `string` status of ad posts ["draft", "submitted", "approved", "scheduled"]

#### Example payload

```
{
    "jsonrpc": "2.0",
    "method": "advertizer.get_ad_posts",
    "params": {
        "name": "imwatsi.test",
        "status": "
    },
    "id": 1
}
```

#### Expected response

```
{
    "jsonrpc": "2.0",
    "result": [
        {
            "permlink": "cool-post-i-wanna-promote-3",
            "created": "2020-09-20T13:39:33",
            "ad_type": "post_global",
            "content": "This is a test... please ignore... Interesting promo content here. [UPDATED] again, and once more",
            "properties": "{}"
        },
        {
            "permlink": "cool-post-i-wanna-promote-5",
            "created": "2020-09-24T14:50:15",
            "ad_type": "post_global",
            "content": 'A "test"... please ignore... [Interesting promo content here].',
            "properties": "{}"
        }
    ],
    "id": 1
}
```

### get_account_notifications

#### Params

`name`: `string` name of Hive account
`category`: `string` (optional) types of notifications ["error"]

#### Example Payload
```
{
    "jsonrpc": "2.0",
    "method": "advertizer.get_account_notifications",
    "params": {
        "name": "imwatsi.test",
        "category": "error"
    },
    "id": 1
}
```

#### Expected Response

```
{
    "jsonrpc": "2.0",
    "result": {
        "notifications": [
            {
                "created": "2020-09-26T22:40:00",
                "category": 0,
                "remark": "you have funded an ad with status 'submitted'; consider contacting the monetizer's management to resolve this"
            },
            {
                "created": "2020-09-26T22:39:57",
                "category": 0,
                "remark": "ad_bid: start_time can't be in the past"
            },
            {
                "created": "2020-09-26T22:20:18",
                "category": 0,
                "remark": "ad_bid: start_time can't be in the past"
            },
            {
                "created": "2020-09-24T15:05:36",
                "category": 0,
                "remark": "can only submit ads that are new or in draft status"
            },
            {
                ...
            }
        ],
        "last_read": null
    },
    "id": 1
}
```